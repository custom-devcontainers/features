# Usage

Leveraging the GitLab Generic Package API, the URL syntax below can be used when including features from this repository.

```
"https://gitlab.com/api/v4/projects/custom-devcontainers%2Ffeatures/packages/generic/${feature_id}/${version}/devcontainer-feature-${feature_id}.tgz"
```