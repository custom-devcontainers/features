#!/usr/bin/env bash

set -e

# Clean up
if [ "$(ls -1 /var/lib/apt/lists/ | wc -l)" -gt -1 ]; then
    rm -rf /var/lib/apt/lists/*
fi

export DEBIAN_FRONTEND=noninteractive

apt-get -y update

apt-get -y install --no-install-recommends \
    clang lld pkg-config libx11-dev libasound2-dev libudev-dev libxkbcommon-x11-0 \
    libwayland-dev libxkbcommon-dev \
    mesa-utils mesa-vulkan-drivers vulkan-tools x11-xserver-utils x11-utils wayland-utils libxkbcommon-tools alsa-utils libasound2-plugins

# Use PulseAudio as ALSA default
ln -sf /etc/alsa/conf.d/99-pulseaudio-default.conf.example /etc/alsa/conf.d/99-pulseaudio-default.conf

curl -sL https://github.com/Jake-Shadle/xwin/releases/download/0.6.6-rc.2/xwin-0.6.6-rc.2-x86_64-unknown-linux-musl.tar.gz | tar --strip-components=1 -C /tmp -zxf - xwin-0.6.6-rc.2-x86_64-unknown-linux-musl/xwin

xwin_path=/usr/local/xwin

/tmp/xwin --accept-license --temp splat --output "${xwin_path}"

rm /tmp/xwin

mkdir -p /.cargo

tee /.cargo/config.toml << EOF
[target.x86_64-unknown-linux-gnu]
linker = "clang"
rustflags = ["-C", "link-arg=-fuse-ld=lld"]

[target.x86_64-pc-windows-msvc]
linker = "lld"
rustflags = [
  "-Lnative=${xwin_path}/crt/lib/x86_64",
  "-Lnative=${xwin_path}/sdk/lib/um/x86_64",
  "-Lnative=${xwin_path}/sdk/lib/ucrt/x86_64"
]
EOF

# Clean up
rm -rf /var/lib/apt/lists/*

echo "Done!"