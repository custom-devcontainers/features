#!/usr/bin/env bash
set -e

v4_api_url="https://gitlab.com/api/v4"
project_id="53998832"
package_name="devcontainer"
package_version="${VERSION:-"0.56.2"}"

curl --remote-name --output-dir /tmp ${v4_api_url}/projects/${project_id}/packages/generic/${package_name}/${package_version}/${package_name}-${package_version}-linux-x64.tar.gz
tar -xzvf /tmp/${package_name}-${package_version}-linux-x64.tar.gz -C /usr/local/bin